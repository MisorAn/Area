﻿using System;
using System.Globalization;

namespace Area
{
    class Program
    {
        class Circle
        {
            public static double Square()
            {
                Console.Write("Введите радиус: ");
                double r = Convert.ToDouble(Console.ReadLine());
                return Math.PI * Math.Pow(r, 2);
            }
        }
        class Rectangle
        {
            public static double Square()
            {
                Console.Write("Введите длину: ");
                double a = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите ширину: ");
                double b = Convert.ToDouble(Console.ReadLine());
                return a*b;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Площадь какой фигуры вы хотите посчитать: круга (введите 0) или прямоугольника (введите 1)?");
            int x = int.Parse(Console.ReadLine());
            double S = Math.Round(((x == 0) ? Circle.Square() : Rectangle.Square()),4);
            Console.WriteLine("Площадь равна " + S);
        }
    }
}
